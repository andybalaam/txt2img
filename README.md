# txt2img

Convert plain text files into images based on an Open Document Text file template.

## Usage

```bash
./txt2img TEMPLATE_ODT TEXT_FILE OUTPUT_PNG
```

Example:

```bash
./txt2img ./doc.odt my_file.txt output.png
```

## Template

Start with the included file doc.odt.  You must have a paragraph of style P1
that contains only the word REPLACEME.  Your text will be inserted instead of
that, with one P1 paragraph per line of text in the supplied text file.

## License

Copyright 2019 Andy Balaam, released under the AGPLv3.  See [LICENSE](LICENSE)
for more information.